package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/http/httptrace"
	"sync"
	"time"
	"vast/parser"
)

const (
	vast_url          = "https://ads.nsc-lab.io/ads/vast/get?out=3&opc=57baef95b53c4b5d6c57d5e8&cid=100&cnt=1&dur=0&adid=&appid=&site_id=171&zone_id=5871&pid=5848"
	workersNum        = 10
	requestsPerSecond = 50
	requestsLimit     = 12 * 1000
	timeout           = 5
)

var (
	p = fmt.Println
	pf = fmt.Printf
)
type handler func(*http.Response) (status int)
type command struct {
	url     string
	handler handler
}
type commandsChan chan *command

type commandStat struct {
	status   int
	duration time.Duration
}

var (
	wg            sync.WaitGroup
	counter       = make(chan commandStat, workersNum)
	commands      = make(commandsChan, requestsPerSecond*10)
	eventCommands = make(commandsChan, workersNum*5)
	quit          = make(chan int, workersNum)
)

func updateCounter(status int, duration time.Duration) {
	//p("Update Counter")
	counter <- commandStat{status, duration}
}

func worker(ctx context.Context, client *http.Client, commandsIn <-chan *command, commandsOut chan<- *command) error  {
	defer wg.Done()

	request := func(url string, handler handler) (int, time.Duration) {
		t0 := time.Now()
		req, err := http.NewRequest("GET", url, nil)
		trace := &httptrace.ClientTrace{
			GotConn: func(connInfo httptrace.GotConnInfo) {
				pf("Got Conn: %+v\n", connInfo)
			},
			DNSDone: func(dnsInfo httptrace.DNSDoneInfo) {
				pf("DNS Info: %+v\n", dnsInfo)
			},
		}
		req = req.WithContext(httptrace.WithClientTrace(req.Context(), trace))
		resp, err := client.Do(req)
		d := time.Since(t0)

		if err != nil {
			p(err)
			return 0, d
		}
		defer resp.Body.Close()

		if resp.StatusCode != 200 {
			p("Status is not 200")
			return resp.StatusCode, d
		}

		//contentType := resp.Header ["Content-Type"]
		//p(contentType)
		//resp.Body.Close()
		//resp = nil
		//tr.CloseIdleConnections()

		return handler(ctx, commandsOut, resp), d
	}

	for {
		var cmdToDo *command
		
		select {
		case cmd, ok := <-commandsIn:
			if !ok {
				break
			}
			status, d = request(cmd.url, cmd.handler)
			updateCounter(ctx, status, d)
		case <-ctx.Done():
			return ctx.Err()
		}
	}

	return error.
}

func addEventRequest(url string) bool {
	doNothing := func(resp *http.Response) (status int) {
		_, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return 0
		}
		return resp.StatusCode
	}

	_ = doNothing

	//eventCommands <- &command{url, doNothing}
	return true
}

func addVASTRequest(ctx context.Context, commandsOut chan<- * command, url string) bool {
	//p("AddVASTRequest")

	parseVASTResponse := func(resp *http.Response) (status int) {
		body, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()

		if err != nil {
			return 0
		}

		vast := parser.Parse(body)

		if vast.Impression == "" {
			p("Empty VAST")
			return 0
		}
		addEventRequest(vast.Impression)
		for _, event := range vast.Creative[0].TrackingEvents {
			switch event.Type {
			case "start", "creativeView", "firstQuartile":
				addEventRequest(event.URL)
			}
		}

		return resp.StatusCode
	}

	//atomic.AddUint64(&requests, 1)
	select {
	case commands <- &command{url, parseVASTResponse}:
		return true
	default:
		//p("AddVASTRequest: Can't add to commands")
		return false
	}
}

func main() {

	dialer := net.Dialer{
		Timeout:   30 * time.Second,
		KeepAlive: 30 * time.Second,
		DualStack: true,
	}

	tr := http.DefaultTransport
	tr.(*http.Transport).DialContext = func(ctx context.Context, network, addr string) (net.Conn, error) {
		if addr == "ads.nsc-lab.io:443" {
			addr = "85.235.174.22:443"
		}
		if addr == "ads.nsc-lab.io:80" {
			addr = "85.235.174.22:80"
		}
		return dialer.DialContext(ctx, network, addr)
	}

	client := &http.Client{
		Transport: tr,
		Timeout:   timeout * time.Second,
	}

	for x := 0; x < workersNum; x++ {
		wg.Add(1)
		go worker(client)
	}

	go func() {
		var (
			getCount = make(map[int]uint)
			c        int64
			totalD   time.Duration
		)

		ticker := time.Tick(2000 * time.Millisecond)

		for {
			select {
			case s := <-counter:
				getCount[s.status]++
				if s.status == 200 {
					c++
					totalD += s.duration
				}
			case <-ticker:
				p("Difference counter: ", getCount)
				if c != 0 {
					p(c/2, " hits, average duration: ", time.Duration(int64(totalD)/c))
				} 
				c, totalD = 0, 0
			}
		}
	}()

	var (
		RPS       = requestsPerSecond
		sendCount = 0
		//incRPS = requestsPerSecond/10
	)
Loop:
	for {
		select {
		case <-time.Tick(1000 * time.Millisecond):
			failCount := 0
			for previousCount := sendCount; sendCount < requestsLimit && sendCount < previousCount+RPS; sendCount++ {
				if !addVASTRequest(vast_url) {
					failCount++
				}
			}

			if sendCount >= requestsLimit {
				break Loop
			}
			/*
				if failCount == 0 {
					RPS += incRPS
				} else {
					RPS -= incRPS
				}
				p("RPS:", RPS)
			*/
		}
	}

	p("Preparing to quit...")
	time.Sleep(5 * time.Second)

	for i := 0; i < workersNum; i++ {
		quit <- 1
	}
	close(commands)
	wg.Wait()
}
